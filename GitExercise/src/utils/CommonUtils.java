package utils;

import java.util.ArrayList;

/**
 * This is a class that will host functions commonly used for development
 * @author Srinivas Kalyani
 *
 */
public class CommonUtils {
	
	/**
	 * This function returns an ArrayList of
	 * all the duplicate elements within a given ArrayList of Integers. Duplicate elements from
	 * the original ArrayList are removed.
	 * 
	 * @param numberList ArrayList of Integers from which duplicates need to be removed
	 * @return ArrayList with all the duplicate numbers
	 */
	public ArrayList<Integer> findDuplicates(ArrayList<Integer> numberList) {
		//Please ensure that the correct ArrayList is returned
		ArrayList<Integer> numberListDuplicate = new ArrayList<>();
		Iterator<Integer> iterator = numberList.iterator();
		HashSet<Integer> listSet = new HashSet<>();
		while (iterator.hasNext()) {
			int value = iterator.next();
			if (!listSet.add(value)) {
				numberListDuplicate.add(value);
				iterator.remove();
			}
		}
		return numberListDuplicate;
	}	
	
	/**
	 * This function sorts a given ArrayList according to bubble sort.
	 * IMPORTANT NOTE 1 - Please write your own bubble sort logic.
	 * IMPORTANT NOTE 2 - Please ensure that the size of the list does not exceed 100. 
	 * A message should be displayed to the user if size of list exceeds 100.
	 * 
	 * @param numberList List on which bubble sort will be applied
	 */
	public void bubbleSort(ArrayList<Integer> numberList) {
		//Bubble sort implementation goes here		
		ArrayList<Integer> unsortedList=numberList;	
		int size = unsortedList.size();
		if(size <= 100)
		{
			do {
				for (int init_element = 0; init_element < size-1; init_element++) {
					if (unsortedList.get(init_element) > unsortedList.get(init_element + 1)) {
						Integer temp1= unsortedList.get(init_element + 1);
						Integer temp2= unsortedList.get(init_element);
						unsortedList.set(init_element,temp1);
						unsortedList.set(init_element+1,temp2);
					}
				}
				size = size - 1;
			} while (size != 1);
		}
		else {
			System.out.println("list size exceeds hundred ");
		}		
	}
	
	/**
	 * This function returns the number of occurrences of a word
	 * @param word Word whose occurrences need to be found
	 * @return number of occurrences for the word
	 */
		public static int findOccurences(String word, String sentence)  
	{ 
	    
	    String splitSentence[] = sentence.split(" "); 
	  
	   
	    int repeat = 0; 
	    for (int search = 0; search < splitSentence.length; search++)  
	    { 
	    
	    if (word.equals(splitSentence[search])) 
	        repeat++; 
	    } 
	  
	    return repeat; 
	} 

}
